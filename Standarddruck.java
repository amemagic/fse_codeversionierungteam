public class Standarddruck implements Druckbar {

    @Override
    public void drucken(Person person) {
        System.out.println(person.getId() + " " + person.getName());;

        if (person instanceof Mitarbeiter){
            System.out.println(((Mitarbeiter) person).getPosition() + ", " + ((Mitarbeiter) person).getEmail());
        }
    }

}
