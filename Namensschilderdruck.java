public class Namensschilderdruck implements Druckbar{

public void drucken (Mitarbeiter mitarbeiter)
{
    System.out.println("* ID: " +mitarbeiter.getID()+ ", Name: "+mitarbeiter.getName());


    String position = mitarbeiter.getPosition();
    int stars = 0;

    if (position.equals("Mitarbeiter")) {
        stars = 1;
    } else if (position.equals("Abteilungsleiter")) {
        stars = 3;
    } else if (position.equals("CEO")) {
        stars = 4;
    }
    for (int i = 0; i < stars; i++) {
        System.out.print("*");
    }
}

}
