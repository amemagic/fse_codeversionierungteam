# Codeversionierung - Team
*Amar Memagic* <br>
*Bernhard Obermoser* <br>
*Lukas Hutz*



## Experiment 1
Alle Teammitglieder pushen Teile eines gemeinsamen Projektes in die Main-Branch. Als Beispiel wird dafür das Druck-Beispiel aus der ersten Übung verwenden. Zum Schluss sollen die Klassen der einzelnen Entwickler zum gesamten Programm zusammenfügen.

**Aufteilung:** <br>
Amar - Main-Klasse <br>
Bernhard - Druckerklassen <br>
Lukas - Personenklassen <br>

**Ablauf:**
1. Amar: Fügt Main.java zum Projekt hinzu
2. Amar: Spielt Änderung über git auf die Main-Branch
```console
git commit -a -m "Main-Klasse"
git push origin main
```
3. Lukas: Fügt Personenklassen zum Projekt hinzu
4. Lukas: Pullt von der Main-Branch und pusht dann die Personenklassen
```console
git pull origin main
git commit -a -m "Personenklassen"
git push origin main
```
5. Bernhard: Fügt Druckerklassen zum Projekt hinzu
6. Bernhard: Pullt von der Main-Branch und pusht dann die Druckerklassen
```console
git pull origin main
git commit -a -m "Druckerklassen"
git push origin main
```

## Experiment 2
Die Teammitglieder teilen sich in verschiedene Branches auf und aktualisieren ihre Klassen in den jeweiligen Branches. Danach werden alle branches gemergt. 

**Aufteilung:** <br>
Amar - Main-Klasse <br>
Bernhard - Druckerklassen <br>
Lukas - Personenklassen <br>

**Ablauf:**
1. Amar: Erstellt neue Branch
```console
git checkout -b Hauptklassenupdate
```
2. Amar: Aktualisiert Main.java
3. Amar: Pusht Änderungen in Branch Hauptklassenupdate
```console
git commit -a -m "Aktualisierung Main-Klasse"
git push origin Hauptklassenupdate
```
4. Lukas: Erstellt neue Branch
```console
git checkout -b PersonenUpdate
```
5. Lukas: Aktualisiert Personenklassen
6. Lukas: Pusht Änderungen in Branch PersonenUpdate
```console
git commit -a -m "Aktualisierung Druckerklassen"
git push origin PersonenUpdate
```
7. Bernhard: Erstellt neue Branch
```console
git checkout -b Druckerupdate
```
8. Bernhard: Aktualisiert Druckerklassen
9. Bernhard: Pusht Änderungen in Branch Druckerupdate
```console
git commit -a -m "Aktualisierung Druckerklassen"
git push origin Druckerupdate
```
10. Mergen der jeweiligen Branches durch folgende Schritte
```console
git checkout main
git merge BranchName // Ersetzen durch Name der jeweiligen Branch
git pull
git push
```
