public class Visitenkartendruck {

    private Mitarbeiter mitarbeiter;

    private Druckbar drucker;

    public Visitenkartendruck(Mitarbeiter mitarbeiter, Druckbar drucker) throws NullPointerException {
        setMitarbeiter(mitarbeiter);
        setDrucker(drucker);
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) throws NullPointerException {
        if (mitarbeiter == null){
            throw new NullPointerException();
        } else {
            this.mitarbeiter = mitarbeiter;
        }
    }

    public void setDrucker(Druckbar drucker) throws NullPointerException {
        if (drucker == null){
            throw new NullPointerException();
        } else {
           this.drucker = drucker;
        }
    }

    public void drucken(){
        drucker.drucken(mitarbeiter);
    }

}
