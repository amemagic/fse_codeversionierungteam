public class Rahmendruck implements Druckbar {

    @Override
    public void drucken(Person person) {
        String zeile1 = "| " + person.getId() + " " + person.getName();
        String rahmeno = ".";
        String rahmenu = "'";

        if(person instanceof Mitarbeiter){
            String zeile2 = "| " + ((Mitarbeiter) person).getPosition() + ", " + ((Mitarbeiter) person).getEmail();

            if(zeile2.length() > zeile1.length()) {
                int abzug = zeile1.length();
                zeile2 += " |";
                for (int i = 0; i < zeile2.length() - 2; i++) {
                    rahmeno += "-";
                    rahmenu += "-";
                }
                for (int i = 0; i < (zeile2.length() - abzug - 2); i++) {
                    zeile1 += " ";
                }
                zeile1 += " |";
                rahmeno += ".";
                rahmenu += "'";

            } else {

                zeile1 += " |";
                int abzug = zeile2.length();
                for (int i = 0; i < zeile1.length() - 2; i++) {
                    rahmeno += "-";
                    rahmenu += "-";
                }
                for (int i = 0; i < (zeile1.length() - abzug - 2); i++){
                    zeile2 += " ";
                }

                zeile2 += " |";
                rahmeno += ".";
                rahmenu += "'";

            }

            System.out.println(rahmeno);
            System.out.println(zeile1);
            System.out.println(zeile2);
            System.out.println(rahmenu);

        } else {

            zeile1 += " |";
            for (int i = 0; i < zeile1.length() - 2; i++) {
                rahmeno += "-";
                rahmenu += "-";
            }
            rahmeno += ".";
            rahmenu += "'";

            System.out.println(rahmeno);
            System.out.println(zeile1);
            System.out.println(rahmenu);
        }



    }


}
