public class Main {

    public static void main(String[] args) {

        Namensschilddruck ndrucker = new Namensschilddruck();

        Mitarbeiter mitarbeiter = new Mitarbeiter(1, "Johan", "johan@email.com", "CEO");

        Visitenkartendruck vdruck;
        try {
            vdruck = new Visitenkartendruck(mitarbeiter, ndrucker);
        } catch (NullPointerException n) {
            throw new RuntimeException(n);
        }

        vdruck.drucken();

        Rahmendruck rdrucker = new Rahmendruck();

        vdruck.setDrucker(rdrucker);

        vdruck.drucken();

    }

}